package edu.ucsd.cse110.client;

import static org.junit.Assert.fail;

import java.net.URISyntaxException;

import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnection;
import org.junit.Assert;
import org.junit.Test;

public class TestChatClient {
	
	@Test
	public void test(){
		try {
			ActiveMQConnection connection = 
					ActiveMQConnection.makeConnection(
					/*Constants.USERNAME, Constants.PASSWORD,*/ Constants.ACTIVEMQ_URL);
	        Assert.assertNotNull(connection);
		} catch (JMSException e) {
			fail();
		} catch (URISyntaxException e) {
			fail();
		}
	}

}
