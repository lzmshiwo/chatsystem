package edu.ucsd.cse110.client;



import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.memory.buffer.MessageQueue;




public class ChatClientApplication {

	
	public static boolean flag = true;
	public static ArrayList<String> messageQueue = new ArrayList();

	
	/*
	 * This inner class is used to make sure we clean up when the client closes
	 */
	static private class CloseHook extends Thread {
		ActiveMQConnection connection;
		private CloseHook(ActiveMQConnection connection) {
			this.connection = connection;
		}
		
		public static Thread registerCloseHook(ActiveMQConnection connection) {
			Thread ret = new CloseHook(connection);
			Runtime.getRuntime().addShutdownHook(ret);
			return ret;
		}
		
		public void run() {
			try {
				System.out.println("Closing ActiveMQ connection");
				connection.close();
			} catch (JMSException e) {
				/* 
				 * This means that the connection was already closed or got 
				 * some error while closing. Given that we are closing the
				 * client we can safely ignore this.
				*/
			}
		}
	}

	/*
	 * This method wires the client class to the messaging platform
	 * Notice that ChatClient does not depend on ActiveMQ (the concrete 
	 * communication platform we use) but just in the standard JMS interface.
	 */
	private static ChatClient wireClient() throws JMSException, URISyntaxException {
		
		ActiveMQConnection connection = 
				ActiveMQConnection.makeConnection(
				/*Constants.USERNAME, Constants.PASSWORD,*/ Constants.ACTIVEMQ_URL);
        connection.start();
        CloseHook.registerCloseHook(connection);
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue destQueue = session.createQueue(Constants.QUEUENAME);
        MessageProducer producer = session.createProducer(destQueue);
        return new ChatClient(producer,session);
	}
	
	public static void main(String[] args) throws InterruptedException {
		try {
			
			InFrame f = new InFrame("ChatRoom");
			f.setVisible(true);
			
			while (true) {
				Thread.currentThread().sleep(1000);
				if (!ChatClientApplication.flag) {
					f.setVisible(false);
					break;
				}

			}
			
			
			/* 
			 * We have some other function wire the ChatClient 
			 * to the communication platform
			 */
			ChatClient client = wireClient();
			/* 
			 * Now we can happily send messages around
			 */
	        
	        ChatFrame CF = new ChatFrame("ChatRoom");
	        CF.setVisible(true);
	        
	        while (true) {
	        	Thread.currentThread().sleep(1000);
	      
	        	while (!ChatClientApplication.messageQueue.isEmpty()) {
	        	
	        		Iterator i =ChatClientApplication.messageQueue.iterator();
	        		while (i.hasNext()){

	        			String m = (String) i.next();
	        			client.send(ChatFrame.User + " : " + m);
	        			ChatClientApplication.messageQueue.remove(m);
	        			
	        			break;
	        		}
	        		break;    
	        	}    	
	        }
	        
			//System.out.println("Message Sent!");	
	        //System.exit(0);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}



class InFrame extends Frame {
	TextField tf1 = null;
	TextField tf2 = null;
	Label l1 = null;
	Label l2 = null;
	
	InFrame(String a) {
		super(a);
		this.setLayout(new FlowLayout());
		this.setBounds(300,300,500,500);
		this.setResizable(false);
		
		tf1 = new TextField(15);
		tf2 = new TextField(15);
		tf1.setFont(new Font("Arial",Font.BOLD,15));
		tf2.setFont(new Font("Arial",Font.BOLD,15));
		
		l1 = new Label("User: ");
		l2 = new Label("Password:");
		Button b = new Button("Enter");
	
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent x) {
				System.exit(0);
			}
		});
		
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				ChatClientApplication.flag= false;
			
			}
		});
		
		tf1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String ID = tf1.getText(); //store ID in instant var
				System.out.println("User: " + ID);
				ChatFrame.User = ID;
			}
			
		});
		
		tf2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Password = tf2.getText(); //store Password in instant var
				System.out.println("Password: " + Password);
				ChatFrame.Password = Password;
			}
			
		});
		
		add(l1); add(tf1); add(l2); 
		add(tf2); add(b); 
		pack();	
		
	}
	
}


class ChatFrame extends Frame {
	TextField tf = null;
	TextArea ta = null;

	boolean flag1 = false;
	public static String User;
	public static String Password;

	
	ChatFrame(String name) {
		
		super(name);

		tf = new TextField(40);
		ta = new TextArea();
		tf.setSize(40, 40);
		tf.setFont(new Font("Arial",Font.BOLD,15));
		ta.setFont(new Font("Arial",Font.BOLD,15));
	
		
		this.add(tf,BorderLayout.SOUTH);
		this.add(ta,BorderLayout.NORTH);
		this.setBounds(300,300,900,900);
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		
		tf.addActionListener(new TextMonitor());
		this.pack();
		this.setVisible(true);
		ta.setEditable(false);
		this.setResizable(false);
	
		
	}
	
	class TextMonitor implements ActionListener {

		public void actionPerformed(ActionEvent ae) {
			String q = tf.getText();
			ChatClientApplication.messageQueue.add(q);
			tf.setText("");
			
		}
	}
	

	
}





