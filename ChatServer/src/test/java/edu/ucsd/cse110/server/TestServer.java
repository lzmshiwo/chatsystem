package edu.ucsd.cse110.server;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestServer {

	@Test
	public void test(){
		AnnotationConfigApplicationContext context = 
		          new AnnotationConfigApplicationContext(ChatServerApplication.class);
		Assert.assertNotNull(context);
	}
	
}